const slider = ()=>{
    //Pictures names
    const pictures = ['carousel/home1.jpg','carousel/home2.jpg','carousel/home3.jpg','carousel/home4.jpg','carousel/home5.jpg']
    //Getting elements
    let imgElement = document.getElementsByClassName("image")[0]
    let right      = document.getElementsByClassName('right-btn')[0]
    let left       = document.getElementsByClassName('left-btn')[0]
    let pagination = document.getElementsByClassName('pagination')[0]
    let playPause  = document.getElementsByClassName('play-pause-btn')[0]

    //Define the state of the slider
    const state = {
        current: 0,
        previous: 0,
        inc(){
            this.previous = this.current
            this.current = (this.current + 1) % pictures.length
        },
        dec(){
            this.previous = this.current
            this.current - 1 < 0 ? this.current = pictures.length-1 : this.current--
        }
    }

    //The default picture is the first one.
    imgElement.src = pictures[state.current]

    //Create pagination icons
    for(let i=0; i<pictures.length; i++){
        let newDiv = document.createElement("div")
        i===state.current ? newDiv.className = "circle active" : newDiv.className = "circle" 
        pagination.appendChild(newDiv)
    }

    //Update pagination function to update when new state
    const updatePagination = ()=>{
        pagination.children[state.previous].classList.remove("active")
        pagination.children[state.current].classList.add("active")
    }
    
    //Next helper function. To be cleaner and to not repeat code
    const next = ()=>{
        return setInterval(()=>{
            state.inc()
            imgElement.src = pictures[state.current]
            updatePagination()
            }, 2500)
    }

    //By default it starts playing
    let playingId = next()

    //Event listeners
    //Right button
    right.addEventListener('click', ()=>{
        state.inc()
        imgElement.src = pictures[state.current]
        updatePagination()
    })
    //Left button
    left.addEventListener('click', ()=>{
        state.dec()
        imgElement.src = pictures[state.current]
        updatePagination()
    })
    //Play & Pause buttons
    playPause.addEventListener('click',()=>{
        //if it is playing, playingId is a number
        if(typeof(playingId) === "number"){
            playPause.firstElementChild.classList.remove("fa-pause")
            playPause.firstElementChild.classList.add("fa-play")
            playingId = clearInterval(playingId); 
        }else{
            playPause.firstElementChild.classList.remove("fa-play")
            playPause.firstElementChild.classList.add("fa-pause")
            playingId = next()
        }
    })

}
slider()
