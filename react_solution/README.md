## SOLUTION 2 (2 hour)

The second implementation i did it with the react library.
Its almost the same but with the react componets aproach and also i load the pictures files with webpack so i this aproach you don't need to touch the code at all if you want to add remove or update a picture.

#### Install and Usage:
```sh
$ cd /solution/react-solution
$ yarn install
$ yarn start
```
Author: SiM (santiago ibanez musielack)