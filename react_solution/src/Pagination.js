import React, { Component } from 'react'

export default class Pagination extends Component {
    render() {
        return (
        <div className="pagination">
            {/*I generate the indicators dynamicaly depending on how may pictures i imported  */}
            {new Array(this.props.currentActive.length).fill('').map((item, index)=>{
                return <div key={index} className={`circle ${this.props.currentActive[index]}`}></div>
            })}
        </div>
        )
    }
}
