import React, { Component } from 'react';
import importAll from './helpers/importAll'
import './Slider.css'
import Pagination from './Pagination'

class Slider extends Component {
    constructor(props){
        super(props)
        this.state = {
            current: 0,
            playing: false,
            playingId: null
        }
        //I imported all home images, by doing this we can just add more pictures to the carousel folder.
        this.homeImages = importAll(require.context('./carousel', false, /home\d*\.(png|jpe?g|svg)$/))
    }
    //increment and decrement functions
    increment = ()=>{
        this.setState((prev)=>{return {current: (prev.current + 1) % this.homeImages.length}})
    }
    decrement = ()=>{
        this.setState((prev)=>{return {current: (prev.current - 1 < 0 ? this.homeImages.length-1 : prev.current-1)}})
    }
    //play and plause event handler
    playPause = ()=>{
        if(this.state.playingId !== null){
            clearInterval(this.state.playingId)
            this.setState({playing: false, playingId: null})
        }else{
            let id = setInterval(()=>{this.increment()}, 2500)
            this.setState({playing: true, playingId: id})
        }
    }
    //By default it starts playing
    componentDidMount(){
        let id = setInterval(()=>{this.increment()}, 2500)
        this.setState({playing: true, playingId: id})
    }

    render() {
      return (
        <div className="Slider">
            <img src={this.homeImages[this.state.current]} className="image"/>
            <div className="Slider-control">
                <button onClick={this.playPause} className="play-pause-btn"><i className={`fa fas ${this.state.playing ? 'fa-pause' : 'fa-play'} fa-2x`}></i></button>
                <button onClick={this.decrement} className="left-btn"><i className="fa fa-chevron-left fa-5x"></i></button>
                <button onClick={this.increment} className="right-btn"><i className="fa fa-chevron-right fa-5x"></i></button>
            </div>
            {/* I have an auxiliary array to keep track of the current indicator and react will only update the previous and the current one */}
            <Pagination currentActive={new Array(this.homeImages.length).fill('').map((item, index)=>{return index===this.state.current ?  'active' : ''})} />
        </div>
      );
    }
  }
  
  export default Slider;