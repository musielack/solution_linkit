# SOLUTION 1 LINKIT (time: 3hs)

This is an implementation of a Recurrent Neural Network for sentiment analysis.
ONLY es6, css, html without any library implementation.

## IMPORTANT:
I got the buttons from fontawesome because the repository didn't have an html file or a css file.

## Getting Started

### Pre-requisites
* npm, yarn

#### Install and Usage:
```sh
$ cd into folder
$ npm run build
open index.html in a browser
```

#### Comments:
The idea was simple i needed an event listener for all the controls (right, left and play-pause).
Then i needed an update function to update the indicators bar.
In my aproach if you neeed to add or remove another picture you just add or remove the picture path in the pictures constant on top of my code.

## SOLUTION 2 (2 hour)

The second implementation i did it with the react library.
Its almost the same but with the react componets aproach and also i load the pictures files with webpack so i this aproach you don't need to touch the code at all if you want to add remove or update a picture.

#### Install and Usage:
```sh
$ cd /solution/react-solution
$ yarn install
$ yarn start
```
Author: SiM (santiago ibanez musielack)
