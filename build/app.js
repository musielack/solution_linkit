'use strict';

var slider = function slider() {
    //Pictures names
    var pictures = ['carousel/home1.jpg', 'carousel/home2.jpg', 'carousel/home3.jpg', 'carousel/home4.jpg', 'carousel/home5.jpg'];
    //Getting elements
    var imgElement = document.getElementsByClassName("image")[0];
    var right = document.getElementsByClassName('right-btn')[0];
    var left = document.getElementsByClassName('left-btn')[0];
    var pagination = document.getElementsByClassName('pagination')[0];
    var playPause = document.getElementsByClassName('play-pause-btn')[0];

    //Define the state of the slider
    var state = {
        current: 0,
        previous: 0,
        inc: function inc() {
            this.previous = this.current;
            this.current = (this.current + 1) % pictures.length;
        },
        dec: function dec() {
            this.previous = this.current;
            this.current - 1 < 0 ? this.current = pictures.length - 1 : this.current--;
        }
    };

    //The default picture is the first one.
    imgElement.src = pictures[state.current];

    //Create pagination icons
    for (var i = 0; i < pictures.length; i++) {
        var newDiv = document.createElement("div");
        i === state.current ? newDiv.className = "circle active" : newDiv.className = "circle";
        pagination.appendChild(newDiv);
    }

    //Update pagination function to update when new state
    var updatePagination = function updatePagination() {
        pagination.children[state.previous].classList.remove("active");
        pagination.children[state.current].classList.add("active");
    };

    //Next helper function. To be cleaner and to not repeat code
    var next = function next() {
        return setInterval(function () {
            state.inc();
            imgElement.src = pictures[state.current];
            updatePagination();
        }, 2500);
    };

    //By default it starts playing
    var playingId = next();

    //Event listeners
    //Right button
    right.addEventListener('click', function () {
        state.inc();
        imgElement.src = pictures[state.current];
        updatePagination();
    });
    //Left button
    left.addEventListener('click', function () {
        state.dec();
        imgElement.src = pictures[state.current];
        updatePagination();
    });
    //Play & Pause buttons
    playPause.addEventListener('click', function () {
        //if it is playing, playingId is a number
        if (typeof playingId === "number") {
            playPause.firstElementChild.classList.remove("fa-pause");
            playPause.firstElementChild.classList.add("fa-play");
            playingId = clearInterval(playingId);
        } else {
            playPause.firstElementChild.classList.remove("fa-play");
            playPause.firstElementChild.classList.add("fa-pause");
            playingId = next();
        }
    });
};
slider();